import numpy as np
import matplotlib.pyplot as plt
import time
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split


def load_data(fname):
    points = np.loadtxt(fname, delimiter='\t')
    y_ = points[:, 5]
    x_ = points[:, 0:5]
    print('data loaded. x1:{} y:{}'.format(x_.shape, y_.shape))
    return x_, y_


def evaluate_cost(x_, y_, params):
    zigma = 0
    n = len(x_)
    for i in range(n):
        # print(x_[i, 0], y_[i])
        x0 = x_[i, 0]
        x1 = x_[i, 1]
        x2 = x_[i, 2]
        x3 = x_[i, 3]
        x4 = x_[i, 4]
        y = y_[i]
        y_dash = \
            params[0] * x0 + \
            params[1] * x1 + \
            params[2] * x2 + \
            params[3] * x3 + \
            params[4] * x4 + \
            params[5]
        diff = y - y_dash if y > y_dash else y_dash - y
        cost = diff * diff
        zigma += cost
    return (zigma / n)


def evaluate_gradient(x_, y_, params):
    zigma_m0 = 0
    zigma_m1 = 0
    zigma_m2 = 0
    zigma_m3 = 0
    zigma_m4 = 0
    zigma_b = 0
    n = len(x_)
    for i in range(n):
        x0 = x_[i, 0]
        x1 = x_[i, 1]
        x2 = x_[i, 2]
        x3 = x_[i, 3]
        x4 = x_[i, 4]
        y = y_[i]
        y_dash = \
            params[0] * x0 + \
            params[1] * x1 + \
            params[2] * x2 + \
            params[3] * x3 + \
            params[4] * x4 + \
            params[5]
        diff = y_dash - y
        zigma_m0 += diff * x0
        zigma_m1 += diff * x1
        zigma_m2 += diff * x2
        zigma_m3 += diff * x3
        zigma_m4 += diff * x4
        zigma_b += diff
    m0_gradient = (2 * (zigma_m0 / n))
    m1_gradient = (2 * (zigma_m1 / n))
    m2_gradient = (2 * (zigma_m2 / n))
    m3_gradient = (2 * (zigma_m3 / n))
    m4_gradient = (2 * (zigma_m4 / n))
    b_gradient = (2 * (zigma_b / n))
    return [m0_gradient, m1_gradient, m2_gradient, m3_gradient, m4_gradient, b_gradient]


def update_params(old_params, grad, alpha):
    new_m0 = old_params[0] - alpha * grad[0]
    new_m1 = old_params[1] - alpha * grad[1]
    new_m2 = old_params[2] - alpha * grad[2]
    new_m3 = old_params[3] - alpha * grad[3]
    new_m4 = old_params[4] - alpha * grad[4]
    new_b = old_params[5] - alpha * grad[5]
    return [new_m0, new_m1, new_m2, new_m3, new_m4, new_b]


def predict(x_, params):
    return \
        params[0] * x_[:, 0] + \
        params[1] * x_[:, 1] + \
        params[2] * x_[:, 2] + \
        params[3] * x_[:, 3] + \
        params[4] * x_[:, 4] + \
        params[5]


# initialize the optimizer
optimizer = {'init_params': np.array([-0.6044061584786626, 16.160353338601276, 13.146805847078394, 10.36729940440176, 0.19994508497949526, 108.52262580564866]),
             'airfoil_params': np.array([-25.235134951503195, -8.333707601137501, -9.692618606142528, 3.987395425674922, -8.784491473856034, 134.45524357710866]),
             'max_iterations': 1000,
             'alpha': 0.07,
             'eps': 0.0000001,
             'inf': 1e10}

if __name__ == "__main__":
    # load data
    x_, y_ = load_data("./airfoil_self_noise.dat")

    x_train, x_test, y_train, y_test = train_test_split(
        x_, y_, test_size=0.33, random_state=42)

    # Normalising
    scaler = MinMaxScaler()
    scaler.fit(x_train)
    x_train = scaler.transform(x_train)
    x_test = scaler.transform(x_test)

    # y_scaler = MinMaxScaler()
    # y_scaler.fit(y_train)
    # y_train = y_scaler.transform(y_train)
    # y_test = y_scaler.transform(y_test)

    # time stamp
    start = time.time()

    # Plot points
    iteration_points = list()
    MSE_points = list()

    learned_params = None
    try:
        # gradient descent
        params = optimizer['init_params']
        old_cost = 1e10
        for iter_ in range(optimizer['max_iterations']):
            # evaluate cost and gradient
            cost = evaluate_cost(x_train, y_train, params)
            grad = evaluate_gradient(x_train, y_train, params)
            # display
            if(iter_ % 10 == 0):
                print('iter: {} cost: {} params: {}'.format(iter_, cost, params))
                iteration_points.append(iter_)
                MSE_points.append(cost)

            # check convergence
            if(abs(old_cost - cost) < optimizer['eps']):
                break
            # udpate parameters
            params = update_params(params, grad, optimizer['alpha'])
            old_cost = cost
        learned_params = params
    except:
        cost = optimizer['inf']

    # final output
    print("\n")
    print('time elapsed: {}'.format(time.time() - start))
    print('RMSE cost at convergence: {} (lower the better)'.format(cost ** (0.5)))
    print("\n")

    # Save plot
    learning = plt.plot(iteration_points, MSE_points)
    plt.xlabel("iterations")
    plt.ylabel("MSE cost")
    plt.savefig("plot2.png")

    # Predicting
    y_predict = predict(x_test, params)

    sample = np.stack([y_test[:5], y_predict[:5]], 1)
    print("Sample predictions")
    print("Actual \t\t Predicted (in decibels)")
    for i in range(len(sample)):
        print("{} {} {} ".format(
            sample[i, 0], "\t", sample[i, 1]))

    test_cost = evaluate_cost(x_test, y_test, params)
    print('RMSE cost at testing: {} (lower the better)'.format(test_cost ** (0.5)))
    print("\n")
