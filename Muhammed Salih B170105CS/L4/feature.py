import numpy as np
import pandas as pd
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import mutual_info_classif
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import precision_score
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.decomposition import PCA

# Getting data
fname = "Dermatology/dermatology.data"
columns = [
    "erythema",
    "scaling",
    "definite borders",
    "itching",
    "koebner phenomenon",
    "polygonal papules",
    "follicular papules",
    "oral mucosal involvement",
    "knee and elbow involvement",
    "scalp involvement",
    "family history, (0 or 1)",
    "melanin incontinence",
    "eosinophils in the infiltrate",
    "PNL infiltrate",
    "fibrosis of the papillary dermis",
    "exocytosis",
    "acanthosis",
    "hyperkeratosis",
    "parakeratosis",
    "clubbing of the rete ridges",
    "elongation of the rete ridges",
    "thinning of the suprapapillary epidermis",
    "spongiform pustule",
    "munro microabcess",
    "focal hypergranulosis",
    "disappearance of the granular layer",
    "vacuolisation and damage of basal layer",
    "spongiosis",
    "saw-tooth appearance of retes",
    "follicular horn plug",
    "perifollicular parakeratosis",
    "inflammatory monoluclear inflitrate",
    "band-like infiltrate",
    "Age (linear)",
    "y"
]
data = pd.read_csv(fname, names=columns)

# splitting X and Y
data = data[data['Age (linear)'] != '?'] # Cleaning rows with unknown ages
X = data.iloc[:, 0:34]
Y = data.iloc[:, 34]

# PCA
def pc_analysis(X, coverage):
    enc = OneHotEncoder()
    X = X.to_numpy()
    X_oneHot = enc.fit_transform(X[:, :-1]).toarray()
    scaler = MinMaxScaler()
    age = scaler.fit_transform(X[:, -1:])
    X_oneHot = np.concatenate((X_oneHot, age), axis=1)

    pca = PCA()
    X_pca = pca.fit_transform(X_oneHot)
    covered = 0
    i = None
    for i in range(len(pca.explained_variance_ratio_)):
        covered += pca.explained_variance_ratio_[i]
        if covered > coverage:
            break
    redused_X = X_oneHot[:, :i+1]
    print("-----------------------------")
    print("Principal components analysis")
    print("-----------------------------")
    print("No of features in original data:", X.shape[1])
    print("No of features after one hot encoding:", X_oneHot.shape[1])
    print("No of principal components covering {}% of variance:".format(coverage * 100), redused_X.shape[1])
    print("-----------------------------")


# Using mutual info classifier for info gain
m = mutual_info_classif(X, Y, discrete_features=True)
igs = {columns[i]: m[i] for i in range(len(columns)-1)}
ranked_igs = sorted(igs.items(), key=lambda x: x[1], reverse=True)

# Displaying ranks obtained by info gain
print("---------------------------------------------")
print("Features ranked according to information gain")
print("---------------------------------------------")

for i in range(len(ranked_igs)):
    print(i+1, "\t", ranked_igs[i][0])
print("---------------------------------------------")
print("\n")

# Feature forward
selected_features = []
scores_using_selected_features = []
remaining_features = columns[:-1]
while len(remaining_features) > 0:
    scores = []
    for i in remaining_features:
        subset = X[selected_features + [i]]
        # Decision tree classification
        X_train, X_test, y_train, y_test = train_test_split(subset, Y, test_size=0.20)
        clf = DecisionTreeClassifier()
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        y_test = y_test.to_numpy()
        score = precision_score(y_test, y_pred, average="micro")
        scores.append((i, score))
    best_feat = max(scores, key=lambda item:item[1])
    selected_features.append(best_feat[0])
    scores_using_selected_features.append(best_feat[1])
    remaining_features.remove(best_feat[0])
print("----------------------------------------------------")
print("Order of features selected in feature forward method")
print("----------------------------------------------------")
for i in range(len(selected_features)):
    print(i+1, selected_features[i])
print(scores_using_selected_features)
print("----------------------------------------------------")
print("\n")
plt.plot(range(1, len(selected_features) + 1), scores_using_selected_features)
plt.title("Scores obtained with each additional feature")
plt.show()

# Feature backward
selected_features = columns[:-1]
scores_using_selected_features = []
eliminated_features = []
# Before elimination score
X_train, X_test, y_train, y_test = train_test_split(subset, Y, test_size=0.20)
clf = DecisionTreeClassifier()
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
y_test = y_test.to_numpy()
score = precision_score(y_test, y_pred, average="micro")
scores_using_selected_features.append(score)
while len(selected_features) > 1:
    scores = []
    for i in selected_features:
        subset = selected_features + []
        subset.remove(i)
        subset = X[subset]
        # Decision tree classification
        X_train, X_test, y_train, y_test = train_test_split(subset, Y, test_size=0.20)
        clf = DecisionTreeClassifier()
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        y_test = y_test.to_numpy()
        score = precision_score(y_test, y_pred, average="micro")
        scores.append((i, score))
    worst_feat = max(scores, key=lambda item:item[1])
    selected_features.remove(worst_feat[0])
    scores_using_selected_features.append(worst_feat[1])
    eliminated_features.append(worst_feat[0])
print("----------------------------------------------------")
print("Order of eliminated in feature backward method")
print("----------------------------------------------------")
for i in range(len(eliminated_features)):
    print(i+1, eliminated_features[i])
print(scores_using_selected_features)
print("----------------------------------------------------")
print("\n")
plt.plot(range(0, len(eliminated_features) + 1), scores_using_selected_features)
plt.title("Scores obtained with elimination of features")
plt.show()

# PCA
pc_analysis(X, 0.85)