import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
import numpy as np
import pandas as pd
import K_means
from sklearn import preprocessing

fname = "Iris data/iris.data"
data = pd.read_csv(fname, skiprows=0, names=list('ABCDE'))
X = data.loc[:, 'B':'D'].to_numpy()

plt.scatter(X[:, 0], X[:, 1], c="magenta")
plt.show()
plt.clf()

# Clusterising
iters = []
square_error = []
for i in range(1, 10):
    k_value = i
    clf = K_means.K_means(k=k_value, max_iter=300, tol=1e-20)
    clf.fit(X)
    clf.predict_cluster(X)
    print(clf.prev_error())
    iters.append(i)
    square_error.append(clf.prev_error() * len(X))    
plt.plot(iters, square_error)
plt.ylabel("square error")
plt.xlabel("Iters")
plt.show()
plt.clf()

k_value = 3
clf = K_means.K_means(k=k_value, max_iter=300, tol=1e-20)
clf.fit(X)
clf.predict_cluster(X)
cluster_means = clf.get_cluster_means()

# Predicting clusters
data_cluster_map = clf.predict_cluster(X)
print("Within cluster sum of squares = ", clf.prev_error()*len(X))
print("Cluster means: \n", cluster_means)

# Labelling
clf.assign_labels(X, data.loc[:, 'E'])

# Counting mistakes
count = 0
for i in range(len(X)):
    if clf.label(data_cluster_map[i]) is not data['E'][i]:
        count += 1
print("Total wrong predictions: ", count)
print("Correctness percentage: ", (1 - count / len(X)) * 100)

# Plotting 2d projection of clusters
plt.scatter(cluster_means[:, 0], cluster_means[:, 1], c="cyan", s=150)
colors = ["red", "green", "blue", "purple"]
for i in range(k_value):
    d = X[[data_cluster_map[j]==i for j in range(X.shape[0])]]
    plt.scatter(d[:, 0], d[:, 1], c=colors[i])
plt.show()
