#!/usr/bin/env python
# coding: utf-8

# # Upload data files (If using colab)
# Required files
# - Google_Stock_Price_Train.csv
# - Google_Stock_Price_Test.csv

# In[1]:


import os
import importlib
train_file_name = 'Google_Stock_Price_Train.csv'
test_file_name = 'Google_Stock_Price_Test.csv'
try:
  colab = importlib.util.find_spec('google.colab')
  if colab is None:
    raise ImportError('Not working in colab')
  
  from google.colab import files
  if not train_file_name in os.listdir():
    print("Please upload", train_file_name)
    files.upload()
  if not test_file_name in os.listdir():
    print("Please upload", test_file_name)
    files.upload()
  print("Files ready")
except ImportError as e:
  pass

if train_file_name not in os.listdir():
    print("Could not find file {} in the current directory".format(train_file_name))
    print("Please ensure the file is present in the current directory before executing the code")
    exit(1)
if test_file_name not in os.listdir():
    print("Could not find file {} in the current directory".format(test_file_name))
    print("Please ensure the file is present in the current directory before executing the code")
    exit(1)


# In[2]:


# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime


# # Data preprocessing

# In[3]:


dataset = pd.read_csv('Google_Stock_Price_Train.csv',index_col="Date",parse_dates=True)

# See sample data
dataset.head()


# In[4]:


# Check for missing data
dataset.isna().any()


# In[5]:


# See dataset information
dataset.info()


# ## Fixing data types
# There are columns having numeric values in american notation (having commas), it should be converted to float for training

# In[6]:


# convert close and volume into float type
dataset["Close"] = dataset["Close"].str.replace(',', '').astype(float)
dataset["Volume"] = dataset["Volume"].str.replace(',', '').astype(float)


# In[7]:


# Plot a sample feature
dataset['Close'].plot(figsize=(16,6))


# ## Training method
# A day's features are predicted based on data from previous 60 days. This is implemented by using a many to many recurrent neural network.

# In[8]:


training_set=dataset[['Open', 'High', 'Low', 'Close', 'Volume']]

# Feature Scaling
from sklearn.preprocessing import MinMaxScaler
sc = MinMaxScaler(feature_range = (0, 1))
training_set_scaled = sc.fit_transform(training_set)

# Creating a data structure with 60 timesteps and 1 output
X_train = []
y_train = []
for i in range(60, 1258):
    X_train.append(training_set_scaled[i-60:i, :])
    y_train.append(training_set_scaled[i, :])
X_train, y_train = np.array(X_train), np.array(y_train)


# # Constructing recurrent neural network

# In[9]:


# Part 2 - Building the RNN

# Importing the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout


# In[10]:


# Initialising the RNN
model = Sequential()

# Adding the first LSTM layer and some Dropout regularisation
model.add(LSTM(units = 256, return_sequences = True, input_shape = (X_train.shape[1], 5)))
model.add(Dropout(0.2))

# Adding a second LSTM layer and some Dropout regularisation
model.add(LSTM(units = 256, return_sequences = True))
model.add(Dropout(0.2))

# Adding a third LSTM layer and some Dropout regularisation
model.add(LSTM(units = 128, return_sequences = True))
model.add(Dropout(0.2))

# Adding a fourth LSTM layer and some Dropout regularisation
model.add(LSTM(units = 64))
model.add(Dropout(0.2))

# Adding the output layer
model.add(Dense(units = 5))


# # Training and evaluation

# In[11]:


# Compiling the RNN
model.compile(optimizer = 'adam', loss = 'mean_squared_error')

# Fitting the RNN to the Training set
model.fit(X_train, y_train, epochs = 5, batch_size = 32, validation_split = 0.2)


# # Preprocessing test data

# In[12]:


# Getting the real stock price of 2017
dataset_test = pd.read_csv('Google_Stock_Price_Test.csv',index_col="Date",parse_dates=True)
dataset_test.head()


# In[13]:


dataset_test.info()


# ## Fixing data types
# There are columns having numeric values in american notation (having commas), it should be converted to float for training

# In[14]:


dataset_test["Volume"] = dataset_test["Volume"].str.replace(',', '').astype(float)


# In[15]:


test_set=dataset_test[['Open', 'High', 'Low', 'Close', 'Volume']]
real_stock_price = dataset_test.values[:, :]


# In[16]:


test_set.info()


# # Forecasting
# Iteratively, a days feature values are predicted using a rolling window of 60 days' data. Each iteration will append its prediction to the known data so that the rolling window can move forward in each iteration.

# In[17]:


# Getting the predicted stock price of 2017
inputs = dataset.values[-60:, :]
inputs = sc.transform(inputs)
inputs = inputs.reshape(1, -1, 5)
predicted_stock_price = None
for i in range(60, 80):
    prediction = model.predict(inputs)
    if type(predicted_stock_price) == type(None):
        predicted_stock_price = prediction
    else:
        predicted_stock_price = np.concatenate([predicted_stock_price, prediction], axis=0)
    inputs = np.concatenate([inputs, prediction.reshape(1, -1, 5)], axis=1)[:, 1:, :]
predicted_stock_price = sc.inverse_transform(predicted_stock_price)
predicted_stock_price=pd.DataFrame(predicted_stock_price)
predicted_stock_price.info()


# # Plotting Forecasts
# A few timesteps of actual stock data is prepended to forecast data for better visualisation of graphs

# In[18]:


forecast_data_expected = np.concatenate([dataset.values[-20:, :], real_stock_price], axis=0)
forecast_data_obtained = np.concatenate([dataset.values[-20:, :], predicted_stock_price], axis=0)


# ## Day opening prices

# In[19]:


plt.clf()
# Visualising the results
plt.plot(forecast_data_expected[:, 0], color = 'red', label = 'Real Google Stock Price: Open')
plt.plot(forecast_data_obtained[:, 0], color = 'blue', label = 'Predicted Google Stock Price: Open')
plt.title('Google Stock Price Prediction')
plt.xlabel('Time')
plt.ylabel('Google Stock Price')
plt.legend()
plt.savefig("open-prices.png")
plt.show()


# ## Day highest prices

# In[20]:


# Visualising the results
plt.plot(forecast_data_expected[:, 1], color = 'red', label = 'Real Google Stock Price: High')
plt.plot(forecast_data_obtained[:, 1], color = 'blue', label = 'Predicted Google Stock Price: High')
plt.title('Google Stock Price Prediction')
plt.xlabel('Time')
plt.ylabel('Google Stock Price')
plt.legend()
plt.savefig("high-prices.png")
plt.show()


# ## Day lowest prices

# In[21]:


# Visualising the results
plt.plot(forecast_data_expected[:, 2], color = 'red', label = 'Real Google Stock Price: Low')
plt.plot(forecast_data_obtained[:, 2], color = 'blue', label = 'Predicted Google Stock Price: Low')
plt.title('Google Stock Price Prediction')
plt.xlabel('Time')
plt.ylabel('Google Stock Price')
plt.legend()
plt.savefig("low-prices.png")
plt.show()


# ## Day closing prices

# In[22]:


# Visualising the results
plt.plot(forecast_data_expected[:, 3], color = 'red', label = 'Real Google Stock Price: Close')
plt.plot(forecast_data_obtained[:, 3], color = 'blue', label = 'Predicted Google Stock Price: Close')
plt.title('Google Stock Price Prediction')
plt.xlabel('Time')
plt.ylabel('Google Stock Price')
plt.legend()
plt.savefig("close-prices.png")
plt.show()


# ## Day volumes

# In[23]:


# Visualising the results
plt.plot(forecast_data_expected[:, 4], color = 'red', label = 'Real Google Stock Volumes')
plt.plot(forecast_data_obtained[:, 4], color = 'blue', label = 'Predicted Google Stock Volumes')
plt.title('Google Stock Prediction')
plt.xlabel('Time')
plt.ylabel('Google Stock Price')
plt.legend()
plt.savefig("volumes.png")
plt.show()


# ##### 
