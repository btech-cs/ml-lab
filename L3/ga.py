import numpy as np
import math
import matplotlib.pyplot as plt

def Y(x):
    # return x ** 3 + 9 # Expected answer 63
    return - ((x-37) ** 2) + 4000 # Expected answer 37

def binary(x, bitlen):
    bin(x).replace("0b","")[-bitlen:]

def num(binary):
    i,integer = 0,0
    size = len(binary)
    while i < len(binary):
        integer += int(binary[size - 1 - i])*pow(2,i)
        i+=1
    return integer

def toss(success_prob):
    x = np.random.rand(1)[0]
    return True if x < success_prob else False

def generate_random_binary(bitlen):
    binary = ""
    for i in range(bitlen):
        binary += "1" if toss(0.5) else "0"
    return binary

def calculate_chances(y):
    sigma = sum(y)
    chances = [None for i in range(len(y))]
    for i in range(len(y)):
        chances[i] = y[i] / sigma
    return chances

def random_int(size):
    x = np.random.rand(1)[0] * size
    return math.floor(x)

def roulette(selection_chances):
    rotation = np.random.rand(1)[0]
    for i in range(len(selection_chances)):
        rotation -= selection_chances[i]
        if (rotation) < 0:
            return i
    return len(selection_chances - 1)

def cross_over(a, b):
    breakpoint = math.floor(np.random.rand(1)[0] * (len(a)+1))
    if (breakpoint < len(a)):
        return a[:breakpoint] + b[breakpoint:]
    else: 
        return a

def mutate(x, probability):
    def invert(symb):
        print("Mutated")
        return "1" if symb == "0" else "0"

    mutated = ""
    for i in x:
        mutated += invert(i) if toss(probability) else i
    return mutated

if __name__ == "__main__":
    
    # Parameters
    generations = 5000 # Indicates maximum, could terminate early
    survival_threshold = 0.2
    cross_over_prob = 1
    mutate_probability = 0.01
    terminal_analysis_gens = math.floor(1 / mutate_probability)
    size = 10
    
    # Plotting function graph
    plt.clf()
    plt.title("Function to be optimised")
    plt.plot([i for i in range(64)], [Y(i) for i in range(64)])
    plt.show()

    # Initializing random population
    x_binary = [None for i in range(size)]

    bitlen = 6
    plotx = []
    ploty = []
    for i in range(len(x_binary)):
        x_binary[i] = generate_random_binary(bitlen)

    print("Initial array: ", x_binary)
    y = None


    prev_samples = []
    peeks_at = 5
    for iteration in range(generations):
        if iteration % peeks_at == 0:
            print("Generation: ", iteration)

        try:

            # Calculate y values
            y_new = np.array([Y(num(x)) for x in x_binary])

            # Roulette selection
            selection_chances = calculate_chances(y_new)
            x_binary_new = [None for i in range(size)]
            for i in range(len(x_binary_new)):
                x_binary_new[i] = x_binary[roulette(selection_chances)]
            
            # Crossover with random mate
            x_crossed_over = [None for i in range(size)]
            for i in range(len(x_binary_new)):
                if toss(cross_over_prob):
                    mate = random_int(size)
                    while mate == i:
                        mate = random_int(size)
                    x_crossed_over[i] = cross_over(x_binary_new[i], x_binary_new[mate])
                else: 
                    x_crossed_over[i] = x_binary_new[i]
            # Mutate
            for i in range(len(x_binary_new)):
                x_crossed_over[i] = mutate(x_crossed_over[i], mutate_probability)

            # Survival selection
            y = np.array([Y(num(x)) for x in x_binary_new])
            y_new = np.array([Y(num(x)) for x in x_crossed_over])
            if (iteration > 0):
                die_out = math.floor(survival_threshold * size)
                worst_soln_indices = y_new.argsort()[0:die_out][::-1]
                best_soln_indices = y.argsort()[-die_out:][::-1]
                for i in range(len(worst_soln_indices)):
                    child = x_crossed_over[worst_soln_indices[i]]
                    parent = x_binary_new[best_soln_indices[i]]
                    competant = child if Y(num(child)) > Y(num(parent)) else parent
                    x_crossed_over[worst_soln_indices[i]] = parent

                if iteration % peeks_at == 0:
                    print("Survived:", [num(x) for x in np.array(x_binary_new)[worst_soln_indices.astype(int)]])
            
            # Replace old population
            x_binary = x_crossed_over
            y = y_new

            # Upgrade termination condition and check
            # topper = max([num(x) for x in x_binary])
            sample = num(x_binary[math.floor(np.random.rand(1)[0] * size)])
            prev_samples.append(sample)
            if iteration > terminal_analysis_gens:
                # Plotting
                terminal_variance = np.var(np.array(prev_samples[-terminal_analysis_gens:]))
                plotx.append(iteration)
                ploty.append(terminal_variance)
                if terminal_variance < mutate_probability:
                    print("Terminated at generation: ", iteration, "\nVariance in highest ranking soln of last {} generations: ".format(terminal_analysis_gens), np.var(np.array(prev_samples[-terminal_analysis_gens:])))
                    break
        except:
            raise
        
    y = np.array([Y(num(x)) for x in x_binary])
    best_of_species = x_binary[np.argmax(y)]
    print("Best of species")
    print("Binary:", best_of_species, "Decimal:", num(best_of_species))
    print("Last generation: ", x_binary)
    print([num(x) for x in x_binary])

    plt.clf()
    plt.title("Evolution")
    plt.plot(plotx, ploty)
    plt.xlabel("Generation")
    plt.ylabel("Variance of random solutions from last {} generations".format(terminal_analysis_gens))
    plt.show()