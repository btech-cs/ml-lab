# Vowel data set
[https://archive.ics.uci.edu/ml/datasets/Japanese+Vowels](https://archive.ics.uci.edu/ml/datasets/Japanese+Vowels)

# Iris data set
[https://archive.ics.uci.edu/ml/datasets/iris](https://archive.ics.uci.edu/ml/datasets/iris)
