import numpy as np
import matplotlib.pyplot as plt
import math
class K_means:
    def __init__(self, k=2, tol=0.00001, max_iter=1000):
        self.k = k
        self.tol = tol
        self.max_iter = max_iter

    def euclid_distance(self, point1, point2):
        square_diff = 0
        for i in range(point1.shape[0]):
            square_diff += (point1[i] - point2[i]) ** 2
        return square_diff ** 0.5

    def fit(self, data):
        rand_indices = [math.floor(i) for i in np.random.rand(self.k) * len(data)]
        self.means = np.copy(data[rand_indices])
        iteration_points = list()
        SE_points = list()
        for t in range(self.max_iter):
            sum_squared = 0
            self.data_cluster_map = np.zeros(data.shape[0], dtype=int)
            for i in range(data.shape[0]):
                distances = [self.euclid_distance(data[i], self.means[j]) for j in range(self.k)]
                self.data_cluster_map[i] = np.argmin(distances)
                sum_squared += distances[np.argmin(distances)] ** 2
            iteration_points.append(t)
            SE_points.append(sum_squared)
            movement = 0
            for i in range(self.k):
                new_mean = np.mean(data[[self.data_cluster_map[j] == i for j in range(data.shape[0])], :], axis=0)
                movement += self.euclid_distance(new_mean, self.means[i])
                self.means[i] = new_mean
            if t % 100 == 0:
                print("Iteration {}".format(t))
            if movement < self.tol:
                break
        plt.plot(iteration_points, SE_points, c="blue")
        plt.xlabel("Iteration")
        plt.ylabel("WCSS")
        # plt.show()
        plt.clf()

    def get_cluster_means(self):
        return self.means

    def get_clusters(self):
        return self.data_cluster_map

    def closest_cluster(self, point):
        distances = [self.euclid_distance(point, self.means[i]) for i in range(self.k)]
        return np.argmin(distances)
    
    def predict_cluster(self, data):
        self.mse = 0
        prediction = np.zeros(data.shape[0], dtype=int)
        for i in range(len(data)):
            prediction[i] = self.closest_cluster(data[i])
            self.mse += self.euclid_distance(data[i], self.means[prediction[i]]) ** 2
        self.mse /= len(data)
        return prediction

    def prev_error(self):
        return self.mse

    def assign_labels(self, data, labels):
        self.labels = [None for i in range(self.k)]
        uniq_labels = set(labels)
        for i in uniq_labels:
            pred = self.predict_cluster(data[[labels[j] == i for j in range(len(data))]])
            self.labels[np.bincount(pred).argmax()] = i
        
    def label(self, i):
        return self.labels[i]

                
        