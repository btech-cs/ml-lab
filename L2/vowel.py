import numpy as np
import K_means
from sklearn.preprocessing import MinMaxScaler

train_file = "Vowel data/ae.train"
train_sizes_file = "Vowel data/size_ae.train"
test_file = "Vowel data/ae.test"
test_sizes_file = "Vowel data/size_ae.test"

train_file = open(train_file)
test_file = open(test_file)
train_sizes = np.loadtxt(train_sizes_file, delimiter=' ', dtype=int)
test_sizes = np.loadtxt(test_sizes_file, delimiter=' ', dtype=int)

people_count = 9
people = list(range(9))

# Reading train data
train_data = train_file.read().split('\n\n')

for i in range(len(train_data)):
    train_data[i] = train_data[i].split('\n')
    for j in range(len(train_data[i])):
        train_data[i][j] = train_data[i][j].split(' ')
        train_data[i][j].pop()
    train_data[i] = np.mean(np.array(train_data[i], dtype="float"), axis=0).tolist()
block = 0
for i in range(len(train_sizes)):
    for j in range(train_sizes[i]):
        train_data[block] = np.append(train_data[block], [i])
        # for k in range(len(train_data[block])):
        #     train_data[block][k].append(i)
        block += 1
# train_data = [train_data[i][j] for i in range(len(train_data)) for j in range(len(train_data[i])-1)]
train_data = np.stack(train_data[:-1], axis=0)

# Reading test data
test_data = test_file.read().split('\n\n')
for i in range(len(test_data)):
    test_data[i] = test_data[i].split('\n')
    for j in range(len(test_data[i])):
        test_data[i][j] = test_data[i][j].split(' ')
        test_data[i][j].pop()
    test_data[i] = np.mean(np.array(test_data[i], dtype="float"), axis=0)
block = 0
for i in range(len(test_sizes)):
    for j in range(test_sizes[i]):
        test_data[block] = np.append(test_data[block], [i])
        block += 1
# test_data = [test_data[i][j] for i in range(len(test_data)) for j in range(len(test_data[i])-1)]
test_data = np.stack(test_data[:-1])

scaler = MinMaxScaler()
X_train = train_data[:, :-1]
X_train = scaler.fit_transform(X_train)
print(X_train.shape)

clf = K_means.K_means(k=9)
# X_train[: , :-1] += 100
clf.fit(X_train)
print(clf.get_cluster_means())

X_test = test_data[:, :-1]
X_test = scaler.transform(X_test)
# X_test[:, :-1] += 100
print(X_test.shape)
clf.predict_cluster(X_train)
print("Within cluster sum of squares = ", clf.prev_error() * len(X_train))
predictions = clf.predict_cluster(X_test)
print(predictions)
print("Within cluster sum of squares (test) = ", clf.prev_error() * len(X_test))

# Labelling
clf.assign_labels(X_test, test_data[:, -1])

# Counting mistakes
count = 0
for i in range(len(X_test)):
    if clf.label(predictions[i]) != test_data[i][-1]:
        count += 1
print("Total wrong predictions: ", count)
print("Correctness percentage: ", (1 - count / len(X_test)) * 100)