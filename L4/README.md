# Problem

- Feature selection (Ranking of feature using variance, information gain (IG))
- Feature forward method and feature backward method
- Feature extraction using principal component analysis (PCA)